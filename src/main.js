import {createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'

import App from './files/Home.vue'
import TableArticle from './components/TableArticle.vue';
import CreateArticle from './files/CreateArticle.vue';
import UpdateArticle from './files/UpdateArticle.vue';
import VueSidebarMenuAkahon from "./components/SidebarArticle.vue";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', component: TableArticle },
        { path: '/createArticle', component: CreateArticle },
        { path: '/updateArticle/:id', component: UpdateArticle},
    ]
});

const app = createApp(App)

app.component('sidebarComponent', VueSidebarMenuAkahon);
app.use(router);
app.mount('#app')